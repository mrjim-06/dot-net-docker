﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

using VOL.Core.Configuration;
using VOL.Core.Extensions;

namespace VOL.Core.Utilities
{
    public class MailService
    {

        public async Task SendEmailAsync(string email, string subject, string message)
        {

            // 设置邮件内容
            var mail = new MailMessage(new MailAddress(AppSetting.Emailsmtp.EmailAddress), new MailAddress(email));
            mail.Subject = subject;
            mail.Body = message;
            mail.IsBodyHtml = true;
            mail.BodyEncoding = Encoding.UTF8;
            mail.Priority = MailPriority.High;//邮件优先级
                                              // 设置SMTP服务器
            var smtp = new SmtpClient("smtp.163.com", 25);
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(AppSetting.Emailsmtp.EmailName, AppSetting.Emailsmtp.EmailPass);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            await smtp.SendMailAsync(mail);
        }


        /// <summary>  
        /// 发送邮件程序调用方法 SendMail("abc@126.com", "某某人", "cba@126.com", "你好", "我测试下邮件", "邮箱登录名", "邮箱密码", "smtp.126.com", true,);  
        /// </summary>  
        /// <param name="from">发送人邮件地址</param>  
        /// <param name="fromname">发送人显示名称</param>  
        /// <param name="to">发送给谁（邮件地址）</param>  
        /// <param name="subject">标题</param>  
        /// <param name="body">内容</param>  
        /// <param name="username">邮件登录名</param>  
        /// <param name="password">邮件密码</param>  
        /// <param name="server">邮件服务器 smtp服务器地址</param>  
        /// <param   name= "IsHtml "> 是否是HTML格式的邮件 </param>  
        /// <returns>send ok</returns>  
        public static bool SendMail(string from, string fromname, string to, string subject, string body, string server, string username, string password, bool IsHtml)
        {

            //指定smtp服务地址（根据发件人邮箱指定对应SMTP服务器地址）
            SmtpClient client = new SmtpClient();//格式：smtp.126.com  smtp.164.com
            client.Host = AppSetting.Emailsmtp.EmailService;//   "smtp.qq.com";
            client.UseDefaultCredentials = true;
            //要用587端口
            //client.Port = int.Parse(AppSetting.Emailsmtp.EmailPort);//587;//端口 
            client.EnableSsl = true;  //加密
            //通过用户名和密码验证发件人身份
            client.Credentials = new NetworkCredential(AppSetting.Emailsmtp.EmailName, AppSetting.Emailsmtp.EmailPass); // 

            MailMessage mailMsg = new MailMessage();//实例化对象
            mailMsg.From = new MailAddress(AppSetting.Emailsmtp.EmailAddress, fromname);//源邮件地址和发件人
            mailMsg.To.Add(new MailAddress(to));//收件人地址
            mailMsg.Subject = subject;//发送邮件的标题;//发送邮件的标题

            mailMsg.Body = body;
            //是否HTML形式发送  
            mailMsg.IsBodyHtml = IsHtml;
            try
            {
                //Console.WriteLine("发送.............2.");
                client.Send(mailMsg);
                //Console.WriteLine("邮件已发送成功，请注意查收！...........3...");
                return true;
            }
            catch (SmtpException ex)
            {
               //Console.WriteLine("邮件发送失败！" + ex.Message);
                return false;
            }
            finally
            {
                client.Dispose();
                mailMsg.Dispose();
            } 
        }

        //读取指定URL地址的HTML，用来以后发送网页用  
        public static string ScreenScrapeHtml(string url)
        {
            //读取stream并且对于中文页面防止乱码  
            StreamReader reader = new StreamReader(System.Net.WebRequest.Create(url).GetResponse().GetResponseStream(), System.Text.Encoding.UTF8);
            string str = reader.ReadToEnd();
            reader.Close();
            return str;
        }

        //发送plaintxt  
        public static bool SendText(string from, string fromname, string to, string subject, string body, string server, string username, string password)
        {
            return SendMail(from, fromname, to, subject, body, server, username, password, false);
        }

        //发送HTML内容  
        public static bool SendHtml(string from, string fromname, string to, string subject, string body, string server, string username, string password)
        {
            return SendMail(from, fromname, to, subject, body, server, username, password, true);
        }

        //发送制定网页  
        public static bool SendWebUrl(string from, string fromname, string to, string subject, string server, string username, string password, string url)
        {
            //发送制定网页  
            return SendHtml(from, fromname, to, subject, ScreenScrapeHtml(url), server, username, password);

        }

        //默认发送格式  
        public static bool SendEmailDefault(string ToEmail, string f_username, string f_pass, string f_times)
        {
            StringBuilder MailContent = new StringBuilder();
            MailContent.Append("亲爱的" + f_username + "会员：<br/>");
            MailContent.Append("    您好！你于");
            MailContent.Append(DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss"));
            MailContent.Append("通过系统管理中心审请邮箱找回密码。<br/>");
            MailContent.Append("　　　为了安全起见，请用户点击以下链接重设个人密码：<br/><br/>");
            var domain = HttpContext.Current.Request.GetAbsoluteUri();
            // var domain = "127.0.0.1:8080";
            string url = domain + "/Rest?u=" + f_username + "&v=" + f_pass + "&t=" + f_times;
            MailContent.Append("<a href='" + url + "'>" + url + "</a><br/><br/>");
            MailContent.Append(" (5分钟后地址失效,如果无法点击该URL链接地址，请将它复制并粘帖到浏览器的地址输入框，然后单击回车即可。)");
            return SendHtml(
                AppSetting.Emailsmtp.EmailName,
               "会员管理中心", ToEmail, f_username + "找回密码", MailContent.ToString(),
                AppSetting.Emailsmtp.EmailService,
                AppSetting.Emailsmtp.EmailName,
                AppSetting.Emailsmtp.EmailPass);
            //这是从webconfig中自己配置的。
        }

        //默认发送格式  
        public static bool SendRegisterEmailDefault(string ToEmail, string f_username, string code, string f_times)
        {
            StringBuilder MailContent = new StringBuilder();
            MailContent.Append("亲爱的"+ f_username + "会员：<br/>");
            MailContent.Append("    您好！你于");
            MailContent.Append(DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss"));
            MailContent.Append("通过注册,获取验证码 。<br/>");
            MailContent.Append("<h2>" + code + "<h2>");
            MailContent.Append("(5分钟后验证码失效。)");
            return SendHtml(
                f_username,
               "会员管理中心", 
                ToEmail,
                f_username + "注册验证码!", 
                MailContent.ToString(),
                AppSetting.Emailsmtp.EmailService,
                AppSetting.Emailsmtp.EmailName,
                AppSetting.Emailsmtp.EmailPass);
        }
    }
}

