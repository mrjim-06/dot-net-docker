﻿using VOL.Core.BaseProvider;
using VOL.Core.Utilities;
using VOL.Entity.DomainModels;
using System.Threading.Tasks;

namespace VOL.System.IServices
{
    public partial interface ISys_UserService
    {

        Task<WebResponseContent> Login(LoginInfo loginInfo, bool verificationCode = false);

        Task<WebResponseContent> ReplaceToken();

        Task<WebResponseContent> ModifyPwd(string oldPwd, string newPwd);

        Task<WebResponseContent> ModifyEmailPwd(string newPwd, string verifyPwd, int userId, string v = "");

        Task<WebResponseContent> GetCurrentUserInfo();

        Task<WebResponseContent> Register(SaveModel saveModel);

       // Sys_User GetUserInfoByAsync(string email);

        Task<WebResponseContent> SendEmailDefault(string email = "");

        Task<WebResponseContent> SendRegisterEmailCodeDefault(string email = "");

      
       // bool SendEmailDefault(string ToEmail, string f_username, string f_pass, string f_times);

       // bool SendEmailDefault(string ToEmail, string f_username, string f_pass, string f_times);

    }
}

