/*
*所有关于Contract_Template类的业务代码接口应在此处编写
*/
using VOL.Core.BaseProvider;
using VOL.Entity.DomainModels;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace VOL.AppManager.IServices
{
    public partial interface IContract_TemplateService
    {
        Task<WebResponseContent> CreatePage(Contract_Template news);
    }
 }
