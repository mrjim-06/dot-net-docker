/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下Contract_SignedDetailsService与IContract_SignedDetailsService中编写
 */
using VOL.AppManager.IRepositories;
using VOL.AppManager.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.AppManager.Services
{
    public partial class Contract_SignedDetailsService : ServiceBase<Contract_SignedDetails, IContract_SignedDetailsRepository>, IContract_SignedDetailsService, IDependency
    {
        public Contract_SignedDetailsService(IContract_SignedDetailsRepository repository)
             : base(repository) 
        { 
           Init(repository);
        }
        public static IContract_SignedDetailsService Instance
        {
           get { return AutofacContainerModule.GetService<IContract_SignedDetailsService>(); }
        }
    }
}
