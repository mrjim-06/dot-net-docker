/*
 *所有关于Contract_SignedDetails类的业务代码应在此处编写
*可使用repository.调用常用方法，获取EF/Dapper等信息
*如果需要事务请使用repository.DbContextBeginTransaction
*也可使用DBServerProvider.手动获取数据库相关信息
*用户信息、权限、角色等使用UserContext.Current操作
*Contract_SignedDetailsService对增、删、改查、导入、导出、审核业务代码扩展参照ServiceFunFilter
*/
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.Utilities;
using System.Linq.Expressions;
using VOL.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using VOL.Core.Enums;
using System.Collections.Generic;

namespace VOL.AppManager.Services
{
    public partial class Contract_SignedDetailsService
    {

        public override PageGridData<Contract_SignedDetails> GetPageData(PageDataOptions options)
        {

             

            //查询前可以自已设定查询表达式的条件
            QueryRelativeExpression = (IQueryable<Contract_SignedDetails> queryable) =>
            {
                return queryable;
            };

            //此处是从前台提交的原生的查询条件，这里可以自己过滤
            QueryRelativeList = (List<SearchParameters> parameters) =>
            {

            };

            //指定多个字段进行排序
            //OrderByExpression = x => new Dictionary<object, QueryOrderBy>() {
            //    { x.CreateDate,QueryOrderBy.Desc },
            //    { x.Enable,QueryOrderBy.Desc}
            //};

            //查询完成后，在返回页面前可对查询的数据进行操作
            GetPageDataOnExecuted = (PageGridData<Contract_SignedDetails> grid) =>
            {
                //可对查询的结果的数据操作
                List<Contract_SignedDetails> sellOrders = grid.rows;
            };

            return base.GetPageData(options);
        }
    }
}
