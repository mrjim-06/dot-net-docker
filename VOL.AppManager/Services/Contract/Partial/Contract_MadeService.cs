/*
 *所有关于Contract_Made类的业务代码应在此处编写
*可使用repository.调用常用方法，获取EF/Dapper等信息
*如果需要事务请使用repository.DbContextBeginTransaction
*也可使用DBServerProvider.手动获取数据库相关信息
*用户信息、权限、角色等使用UserContext.Current操作
*Contract_MadeService对增、删、改查、导入、导出、审核业务代码扩展参照ServiceFunFilter
*/
using VOL.Entity.DomainModels;
using System.Linq;
using VOL.Core.Utilities;
using VOL.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using VOL.AppManager.Repositories;
using System;
using System.Collections.Generic;
using VOL.Core.Enums;
using VOL.Core.DBManager;

namespace VOL.AppManager.Services
{
    public partial class Contract_MadeService
    {
        /// <summary>
        /// 生成合同编号
        /// </summary>
        /// <param name="type">合同类别</param>
        /// <returns></returns>
        public string GetMadeNo(string type)
        {
            string value = type;
            //获取查询到的总和数
            int total = Contract_MadeRepository.Instance.FindAsIQueryable(x => 1 == 1).Count();

            var no_rule = Contract_TemplateRepository.Instance.FindAsIQueryable(x => x.Type == type).FirstOrDefault();

            var date = DateTime.Now.ToString("yyyyMMdd");

            string str = total.ToString().PadLeft(no_rule != null ? no_rule.No : 1, '0');

            return value + date + str;

        }

        /// <summary>
        /// 自定义保存从表数据逻辑
        /// </summary>
        /// <param name="saveDataModel"></param>
        /// <returns></returns>
        public override WebResponseContent Add(SaveModel saveDataModel)
        {
            WebResponseContent responseData = new WebResponseContent();
            base.AddOnExecute = (SaveModel userModel) =>
            {
                string type = userModel?.MainData?["Type"].ToString();

                userModel.MainData["MadeNo"] = this.GetMadeNo(type);

                return responseData.OK();
            }; 
            return base.Add(saveDataModel);
        }

        public PageGridData<Contract_Made> Get(PageDataOptions options)
        {
           

            List<SearchParameters> searchParametersList = new List<SearchParameters>();
            if (!string.IsNullOrEmpty(options.Wheres))
            {
                try
                {
                    searchParametersList = options.Wheres.DeserializeObject<List<SearchParameters>>();
                }
                catch {
                    // return WebResponseContent.Instance.Data = "数据个是不正确";
                   //.Error("请上传文件");
                }
            }
            var sql =$"select DISTINCT t1.Contract_Id from  Contract_SignedDetails t1  LEFT JOIN Contract_Made t2 on t1.Contract_Id = t2.Contract_Id where 1=1 ";

            foreach (var item in searchParametersList)
            {
                if (item.Name == "SignatoryID")
                {
                    if (!string.IsNullOrEmpty(item.Value))
                    {
                        sql += $" and t1.SignatoryID={item.Value}";
                    }
                }
                if (item.Name == "SignatureStatus")
                {
                    sql += $" and t1.SignatureStatus={item.Value}";
                }
                if (item.Name == "Enable")
                {
                    sql += $" and t1.Enable={item.Value}";
                }
            }

            List<Contract_SignedDetails> list = Contract_SignedDetailsRepository.Instance.DapperContext.QueryList<Contract_SignedDetails>(sql, null)
            .Skip(((options.Page == 0 ? 1 : options.Page) - 1) * (options.Rows ==0?10: options.Rows))
            .Take((options.Rows == 0 ? 10 : options.Rows)).ToList();

            var sqlContract_Made = $"select * from  Contract_Made t1 where 1=1 ";
            if (list.Any())
            {
                sqlContract_Made += $" and t1.Contract_Id in ({ string.Join(",", list.Select(s => s.Contract_Id))})";

                var ContractMade = Contract_MadeRepository.Instance.DapperContext.QueryList<Contract_Made>(sqlContract_Made, null).OrderByDescending(s => s.CreateDate).ToList();

                var sqlSignedDetails = $"select * from  Contract_SignedDetails t1 where 1=1  ";

                if (list.Any())
                {
                    sqlSignedDetails += $" and t1.Contract_Id in ({string.Join(",", ContractMade.Select(s => s.Contract_Id))})";
                }
                 

                var SignedDetailsdata = Contract_SignedDetailsRepository.Instance.DapperContext.QueryList<Contract_SignedDetails>(sqlSignedDetails, null);

                foreach (var item in ContractMade)
                {
                    item.Contract_SignedDetails = SignedDetailsdata.Where(w => w.Contract_Id == item.Contract_Id).OrderBy(o => o.SideBy).ToList();
                }

                return new PageGridData<Contract_Made>()
                {
                    rows = ContractMade,
                    total = list.Count(),
                };

            }
            return new PageGridData<Contract_Made>()
            {
                rows = new List<Contract_Made>() { },
                total = list.Count(),
            };

        }
    

        public override PageGridData<Contract_Made> GetPageData(PageDataOptions options)
        {
            //查询前可以自已设定查询表达式的条件
            QueryRelativeExpression = (IQueryable<Contract_Made> queryable) =>
            {

                return queryable.Include(x => x.Contract_SignedDetails);
            };

            //此处是从前台提交的原生的查询条件，这里可以自己过滤
            QueryRelativeList = (List<SearchParameters> parameters) =>
            {

            };
            //指定多个字段进行排序
            OrderByExpression = x => new Dictionary<object, QueryOrderBy>() {
                { x.CreateDate,QueryOrderBy.Desc },
                { x.Enable,QueryOrderBy.Desc}
            };

            //查询完成后，在返回页面前可对查询的数据进行操作
            GetPageDataOnExecuted = (PageGridData<Contract_Made> grid) =>
            {
                //可对查询的结果的数据操作
                List<Contract_Made> sellOrders = grid.rows;
            };

            return base.GetPageData(options);
        }


        public PageGridData<Contract_Made> GetData(PageDataOptions options)
        {

            var sql = "select t1.Contract_Id from  Contract_SignedDetails t1  LEFT JOIN Contract_Made t2 on t1.Contract_Id = t2.Contract_Id where t1.CreateID = 1;";

            var list = DBServerProvider.SqlDapper.QueryList<Contract_Made>(sql, null).Select(s => new Contract_Made
            {
               Contract_Id= s.Contract_Id
            }) ;

            QueryRelativeExpression = (IQueryable<Contract_Made> queryable) =>
            {
                return queryable.Include(x => x.Contract_SignedDetails);
            };

            var d = DBServerProvider.DbContext
                  .Set<Contract_Made>()
                  .Include(c => c.Contract_SignedDetails);

            //var blogs = context.Blogs
            //    .FromSqlRaw("SELECT * FROM dbo.Blogs")
            //    .ToList();
            //string[] partnoArr = MPSList.Select(mps => mps.PartNo).ToArray();
            //var q = DBCtx.KB_BOMTable.Where(ent => partnoArr.Contains(ent.PartNo));
            //查询前可以自已设定查询表达式的条件
            QueryRelativeExpression = (IQueryable<Contract_Made> queryable) =>
            {
                return queryable.Include(x => x.Contract_SignedDetails);
            };


            //此处是从前台提交的原生的查询条件，这里可以自己过滤
            QueryRelativeList = (List<SearchParameters> parameters) =>
            {

            };
            //指定多个字段进行排序
            OrderByExpression = x => new Dictionary<object, QueryOrderBy>() {
                { x.CreateDate,QueryOrderBy.Desc },
                { x.Enable,QueryOrderBy.Desc}
            };

            //查询完成后，在返回页面前可对查询的数据进行操作
            GetPageDataOnExecuted = (PageGridData<Contract_Made> grid) =>
            {
                //可对查询的结果的数据操作
                List<Contract_Made> sellOrders = grid.rows;
            };

            return base.GetPageData(options);

        }


        /// <summary>
        /// 查询业务代码编写
        /// </summary>
        /// <param name="pageData"></param>
        /// <returns></returns>
        //public override object GetDetailPage(PageDataOptions pageData)
        //{
        //    return base.GetDetailPage(pageData);
        //}

        //public virtual object GetDetailPage(PageDataOptions pageData)
        //{
        //    Type detailType = typeof(T).GetCustomAttribute<EntityAttribute>()?.DetailTable?[0];
        //    if (detailType == null)
        //    {
        //        return null;
        //    }
        //    object obj = typeof(ServiceBase<T, TRepository>)
        //         .GetMethod("GetDetailPage", BindingFlags.Instance | BindingFlags.NonPublic)
        //         .MakeGenericMethod(new Type[] { detailType }).Invoke(this, new object[] { pageData });
        //    return obj;
        //}
        /// <summary>
        /// 修改用户拦截过滤
        /// 
        /// </summary>
        /// <param name="saveModel"></param>
        /// <returns></returns>
        //public override WebResponseContent Update(SaveModel saveModel)
        //{ 
        //    return base.Update(saveModel);
        //}
    }
}
