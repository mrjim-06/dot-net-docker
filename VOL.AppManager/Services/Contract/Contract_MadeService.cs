/*
 *Author：jxx
 *Contact：283591387@qq.com
 *代码由框架生成,此处任何更改都可能导致被代码生成器覆盖
 *所有业务编写全部应在Partial文件夹下Contract_MadeService与IContract_MadeService中编写
 */
using VOL.AppManager.IRepositories;
using VOL.AppManager.IServices;
using VOL.Core.BaseProvider;
using VOL.Core.Extensions.AutofacManager;
using VOL.Entity.DomainModels;

namespace VOL.AppManager.Services
{
    public partial class Contract_MadeService : ServiceBase<Contract_Made, IContract_MadeRepository>, IContract_MadeService, IDependency
    {
        public Contract_MadeService(IContract_MadeRepository repository)
             : base(repository) 
        { 
           Init(repository);
        }
        public static IContract_MadeService Instance
        {
           get { return AutofacContainerModule.GetService<IContract_MadeService>(); }
        }
    }
}
