
# 添加签订合同
* 地址:  http://129.204.95.7:9991/api/Contract_Made/add
* 参数说明 
* mainData :{} 合同主题内容
* detailData :{} 合同明细
```
{
  "mainData": {
    "MadeNo": "xxx11", //合同编号
    "Name": "111", //合同名称
    "Type": "2", //合同类别
    "Enable": "1", //是否默认1
    "Content": "23123123123", //合同内容 
    "Creator": "22",//创建人姓名
    "CreateID": 1 //创建人Id
  },
  "detailData": [
    {
      "Signedbyd": "1", //对象(1甲方 2乙方)
      "Payment": 1, //  付款方(1是 0 否)
      "Enable": 0, //是否有效
      "elementIdex": "0", //序号 
      "Creator": "22",//创建人姓名
      "CreateID": 1, //创建人Id
      "LiaisonMain": "222",
      "Mobile": "22222",
      "Address": "aaa",
      "BankAccount": "111",
      "OpeningBank": "zgyx",
      "SignatureUrl":"", //签名图片
      "SignatureStatus":""
    },
    {
      "Signedbyd": "2",//对象(2乙方)
      "Payment": "0",//付款方(1是 0 否)
      "Enable": 0,
      "elementIdex": 1, 
      "Creator": "22",//创建人姓名
      "CreateID": 1, //创建人Id
      "LiaisonMain": "111",
      "Mobile": "22",
      "Address": "bvv",
      "BankAccount": "111",
      "OpeningBank": "xgty",
      "SignatureUrl":"", //签名图片
      "SignatureStatus":""
    }
  ],
  "delKeys": null
}
```

# 更新签订合同 
*  地址:  http://129.204.95.7:9991/api/Contract_Made/update
*  参数说明 
*  mainData :{} 合同主题内容 注意这里有带有主键 Contract_Id
*  detailData :{} 合同明细 注意这里有带有 主键Made_Id  外键 Contract_Id
```
{
  "mainData": {
    "MadeNo": "xxx11",
    "Name": "111",
    "Type": "2",
    "Enable": "1",
    "Contract_Id": "4" //主键ID更新Id 
  }, //签订明细
  "detailData": [
    {
      "Signedbyd": 1,
      "Payment": 0,
      "Creator": "超级管理员", //创建人
      "CreateDate": "2020-06-06 01:12:52",
      "CreateID": 1, //创建人Id
      "LiaisonMain": "22", //联系人
      "Mobile": "123", //联系电话
      "Address": "xx1", //地址
      "BankAccount": 123, //账户
      "OpeningBank": "aaa", //开户行
      "SignatureUrl": null, //签名地址
      "PayerId": null, //付款人ID  
      "Payer": null, //付款人名
      "Enable": 0, //是否有效
      "SignatureStatus": null, //签订状态  1  起草  2签订 (上传签名 更新状态
      "Made_Id": 6, //主键ID更新Id 
      "Contract_Id": 4, //所属合同
      "elementIdex": 0
    },
    {
      "Signedbyd": 2,
      "Payment": 0,
      "Creator": "超级管理员",
      "CreateDate": "2020-06-06 01:12:52",
      "CreateID": 1,
      "LiaisonMain": "333",
      "Mobile": "123",
      "Address": "xx2",
      "BankAccount": 123,
      "OpeningBank": "sz",
      "SignatureUrl": null,
      "PayerId": null,
      "Payer": null,
      "Enable": 0,
      "SignatureStatus": null,
      "Made_Id": 7, //主键ID更新Id 
      "Contract_Id": 4, //所属合同
      "elementIdex": 1
    }
  ],
  "delKeys": null
}
```

# 单个人 新增 签订明细   
* 地址: http://129.204.95.7:9991/api/Contract_SignedDetails/Add    
* 参数说明
*  mainData :{} 合同主题内容 注意这里  带有合同主键 Contract_Id  
*  detailData :{}  无

```
{
  "mainData": {
    "Signedbyd": "1",
    "Payment": 1,
    "Enable": 0,
    "Creator": "超级管理员",
    "LiaisonMain": "222",
    "Mobile": "22222",
    "Address": "aaa",
    "BankAccount": "111",
    "OpeningBank": "zgyx",
    "SignatureUrl": "", 
    "Contract_Id": "4" //外键 合同ID
  },
  "detailData": [],
  "delKeys": null
}
```

# 单个人 更新 签订明细   
* 地址:  http://129.204.95.7:9991/api/Contract_SignedDetails/update  
* 参数说明 
* mainData :{} 合同明细 注意这里 有带主键Made_Id  外键Contract_Id
```
{
    "mainData": {
        "Signedbyd": "1",
        "Payment": 1,
        "Enable": 0,
        "Creator": "超级管理员",
        "LiaisonMain": "222",
        "Mobile": "22222",
        "Address": "aaa",
        "BankAccount": "111",
        "OpeningBank": "zgyx",
        "SignatureUrl": "",
        "Made_Id": "8", //主键ID
       "Contract_Id": "4"//外键合同ID
    },
    "detailData": [
        
    ],
    "delKeys": null
}
```


# 用户注册   
* 地址:  http://129.204.95.7:9991/api/User/register     
* 参数说明 
* mainData :{} 用户信息新增内容 注意这里  没有主键 
* detailData :{}  
```
{
  "mainData": { 
    "UserName": "1",
    "UserPwd":"1qaz23",
    "Gender": 0, //性别//必填
    "HeadImageUrl": "Upload/Tables/Sys_User/202005182223395391/TIM截图20200506214640.png", //头像
    "Dept_Id": null,
    "DeptName": null,
    "Role_Id": 3, //必填3
    "RoleName": "小编",
    "Token": null,
    "AppType": null,
    "UserTrueName": "1", //真实姓名
    "CreateDate": "2020-05-18 22:23:42",
    "IsRegregisterPhone": 0, //必填 
    "PhoneNo": null, // 联系电话
    "Tel": null,
    "CreateID": 1,
    "Creator": "超级管理员",
    "Enable": 1, //必填 是否启用1
    "ModifyID": null,
    "Modifier": null,
    "ModifyDate": null,
    "AuditStatus": 1,
    "Auditor": "超级管理员",
    "AuditDate": "2020-05-22 21:57:26",
    "LastLoginDate": null,
    "LastModifyPwdDate": null,
    "Address": null, //地址
    "Mobile": null, //手机
    "Email": null, //邮箱 必填 邮件需要
    "Remark": "11",
    "OrderNo": null,
    "FrontOfIDCard": null, //身份证正面
    "ReverseOfIDCard": null, //身份证反面
    "BankAccount": null, //银行帐号
    "OpeningBank": null //开户行
  },
  "detailData": [],
  "delKeys": null,
  "extra": { 
     "VerificationCode": ""//邮箱验证码
  }
}
```

# 用户修改 
* 地址:  http://129.204.95.7:9991/api/User/update  
* 参数说明 
* mainData :{} 用户信息修改内容 注意这里   有主键 User_Id
* detailData :{}  

```
{
  "mainData": {
    "User_Id": 3372, //主键ID 注册是不需要,修改是需要
    "UserName": "1",
    "Gender": 0, //性别//必填
    "HeadImageUrl": "Upload/Tables/Sys_User/202005182223395391/TIM截图20200506214640.png", //头像
    "Dept_Id": null,
    "DeptName": null,
    "Role_Id": 3, //必填3
    "RoleName": "小编",
    "Token": null,
    "AppType": null,
    "UserTrueName": "1", //真实姓名
    "CreateDate": "2020-05-18 22:23:42",
    "IsRegregisterPhone": 0, //必填 
    "PhoneNo": null, //是否启用1
    "Tel": null,
    "CreateID": 1,
    "Creator": "超级管理员",
    "Enable": 1, //必填 是否启用1
    "ModifyID": null,
    "Modifier": null,
    "ModifyDate": null,
    "AuditStatus": 1,
    "Auditor": "超级管理员",
    "AuditDate": "2020-05-22 21:57:26",
    "LastLoginDate": null,
    "LastModifyPwdDate": null,
    "Address": null, //地址
    "Mobile": null, //手机
    "Email": null, //邮箱
    "Remark": "11",
    "OrderNo": null,
    "FrontOfIDCard": null, //身份证正面
    "ReverseOfIDCard": null, //身份证反面
    "BankAccount": null, //银行帐号
    "OpeningBank": null //开户行
  },
  "detailData": [],
  "delKeys": null
}
```

# 查询签订合同(及内容)  
* 地址:  /api/Contract_Made/GetPageData 所有获取   明细   字段,查询合同
* 参数说明
* wheres 可以根据动态添加  
*  {\"name\":\"CreateID\",\"value\":\"1\"}  CreateID  创建人者
*  {\"name\":\"Enable\",\"value\":\"1\"}  Enable  1 "有效合同 2 外键 合同明细外键
*  {\"name\":\"Contract_Id\",\"value\":\"1\"}  Contract_Id  合同主外 查询合同内容 
```
{
  "page": 1,
  "rows": 30,
  "sort": "Contract_Id",
  "order": "desc",
  "wheres": "[{\"name\":\"Enable\",\"value\":\"1\"},{\"name\":\"Contract_Id\",\"value\":\"1\"}]" 
}
```

# 查询签订合同明细 已完成 
* 地址:  /api/Contract_Made/GetDetailPage  查询所有签订合同明细  
* 参数说明
* wheres 可以根据动态添加
* value :Contract_Id 

```
{
 order: "desc",
 page: 1,
 rows: 30,
 sort: "CreateDate",
 value: 22,
 wheres: "[]",
}
```


# 查询签订合同明细 已完成 
* 地址:  api/Contract_SignedDetails/GetPageData   查询所有签订合同明细  
* 参数说明
* wheres 可以根据动态添加  

*  {\"name\":\"Contract_Id\",\"value\":\"1\"} CreateID 是否有效
*  {\"name\":\"Enable\",\"value\":\"1\"} CreateID 是否有效
*  {\"name\":\"SignatureStatus\",\"value\":\"1\"} 签订状态  SignatureStatus 1表示 我发起 我已经签订已完成  SignatureStatus 0表示 我需要签订 我未已完成(代签定)
*  {\"name\":\"CreateID\",\"value\":\"1\"} CreateID 创建者(签订者)查询合同 
* "wheres": "[{\"name\":\"SignatureStatus\",\"value\":\"1\"},{\"name\":\"CreateID\",\"value\":\"1\"},{\"name\":\"Enable\",\"value\":\"1\"}]"  

```
{
  "page": 1,
  "rows": 30,
  "sort": "Contract_Id",
  "order": "desc",
  "wheres": "[{\"name\":\"SignatureStatus\",\"value\":\"0\"},{\"name\":\"CreateID\",\"value\":\"1\"}]" // "[{\"name\":\"CreateID\",\"value\":\"1\"},{\"name\":\"Enable\",\"value\":\"1\"}]"  创建者(签订者)id
}
```


 
# 查询所有 合同 模版
* 地址: api/Contract_Template/GetPageData   查询所有合同范文    
* 参数同上 
*  {\"name\":\"Enable\",\"value\":\"1\"}  Enable  1 "有效合同 2 外键 合同明细外键
*  {\"name\":\"Contract_Id\",\"value\":\"1\"}  Contract_Id  合同主外 查询合同内容 
```
{
  "page": 1,
  "rows": 30,
  "sort": "Contract_Id",
  "order": "desc",
  "wheres": "[{\"name\":\"Enable\",\"value\":\"1\"}]" //  有效合同 创建者id
}
```

# 查询所有 合同类别
* 地址:  api/Sys_Dictionary/GetVueDictionary 
* 参数说明
* ["contract"]  查询 key合同类别名称    

```
{
  "page": 1,
  "rows": 30,  
  "wheres": "[{\"name\":\"Enable\",\"value\":\"1\"}]" //  有效合同 创建者id
}
```