﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using VOL.Core.Configuration;
using VOL.Core.Controllers.Basic;
using VOL.Core.DBManager;
using VOL.Core.EFDbContext;
using VOL.Core.Enums;
using VOL.Core.Extensions;
using VOL.Core.Filters;
using VOL.Core.Infrastructure;
using VOL.Core.ObjectActionValidator;
using VOL.Core.Utilities;
using VOL.Entity.AttributeManager;
using VOL.Entity.DomainModels;
using VOL.System.IServices;

namespace VOL.System.Controllers
{
   // [Route("api/User")]
    public partial class Sys_UserController
    {
        [HttpPost, HttpGet, Route("login"), AllowAnonymous]
        [ObjectModelValidatorFilter(ValidatorModel.Login)]
        public async Task<IActionResult> Login([FromBody]LoginInfo loginInfo)
        {
            return Json(await Service.Login(loginInfo));
        }

        [HttpPost, Route("replaceToken"), AllowAnonymous]
        public async Task<IActionResult> ReplaceToken()
        {
            return Json(await Service.ReplaceToken());
        }

        [HttpPost, Route("modifyPwd")]
        [ApiActionPermission]
        //通过ObjectGeneralValidatorFilter校验参数，不再需要if esle判断OldPwd与NewPwd参数
        [ObjectGeneralValidatorFilter(ValidatorGeneral.OldPwd, ValidatorGeneral.NewPwd)]
        public async Task<IActionResult> ModifyPwd(string oldPwd, string newPwd)
        {
            return Json(await Service.ModifyPwd(oldPwd, newPwd));
        }

        [HttpPost, Route("getCurrentUserInfo")]
        public async Task<IActionResult> GetCurrentUserInfo()
        {
            return Json(await Service.GetCurrentUserInfo());
        }

        /// <summary>
        /// 忘记密码,发送邮件找回密码
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet, Route("sendMail"), AllowAnonymous]
        public async Task<IActionResult> SendEmail(string email)
        {
            return Json(await Service.SendEmailDefault(email));
        }


        /// <summary>
        /// 注册发送邮件验证码
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet, Route("SendEmailVerificationCode"), AllowAnonymous]
        public async Task<IActionResult> SendEmailVerificationCode(string email)
        {
            return Json(await Service.SendRegisterEmailCodeDefault(email));
        }

        /// <summary>
        /// 用户注册
        /// </summary>
        /// <param name="saveModel"></param>
        /// <returns></returns>
        [HttpPost, Route("register"), AllowAnonymous]
        public async Task<IActionResult> Register([FromBody]SaveModel saveModel)
        {
            return Json(await Service.Register(saveModel));
        }

        /// <summary>
        /// 用户注册上传文件
        /// </summary>
        /// <param name="fileInput"></param>
        /// <returns></returns>
        [HttpPost, Route("registerUpload"), AllowAnonymous]
        public async Task<IActionResult> RegisterUpload(IEnumerable<IFormFile> fileInput)
        {
            return Json(await base.Upload(fileInput.ToList()));
        }
     
        /// <summary>
        /// 忘记密码,发送邮件找回密码
        /// </summary>
        // <param name = "email" ></ param >
        /// < returns ></ returns >
        [HttpPost, Route("ModifRest"), AllowAnonymous]
        [ObjectModelValidatorFilter(ValidatorModel.Login)]
        public async Task<IActionResult> Rest([FromBody]ModifyEmailPwd modify)
        {
            return Json(await Service.ModifyEmailPwd(modify.newpassword, modify.password, modify.u, modify.v));
        }
        //public async Task<IActionResult> Rest(string newpassword, string passWord, int u, string v)
        //{
        //    return Json(await Service.ModifyEmailPwd(newpassword, passWord, u, v));
        //}

    }
}
