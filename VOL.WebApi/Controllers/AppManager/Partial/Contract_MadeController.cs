/*
 *接口编写处...
*如果接口需要做Action的权限验证，请在Action上使用属性
*如: [ApiActionPermission("Contract_Made",Enums.ActionPermissionOptions.Search)]
 */
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VOL.Core.Enums;
using VOL.Core.Filters;
using VOL.Entity.DomainModels;

namespace VOL.AppManager.Controllers
{
    public partial class Contract_MadeController
    {

        //[ApiActionPermission(ActionPermissionOptions.Search)]
        [HttpPost, Route("GetMatterData"),AllowAnonymous]
        public IActionResult GetData([FromBody] PageDataOptions loadData)
        {
            return Json(Service.Get(loadData));
        }

        //[HttpPost, Route("GetDataDD"), AllowAnonymous]
        //public IActionResult GetDataDD([FromBody] PageDataOptions loadData)
        //{
        //    //Service.GetData(loadData);
        //    return Json(Service.GetData(loadData));
        //}

        /// <summary>
        /// 获取编号
        /// </summary>
        /// <returns></returns>
        //[HttpGet, HttpPost, Route("getMadeNo")]
        ////2019.10.24屏蔽用户查询自己权限菜单
        //// [ApiActionPermission("Sys_Menu", ActionPermissionOptions.Search)]
        //public async Task<IActionResult> GetMadeNo(string type)
        //{
        //    return Json(await Service.GetMadeNo(type));
        //}

    }
}
