/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹Contract_TemplateController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.AppManager.IServices;
namespace VOL.AppManager.Controllers
{
    [Route("api/Contract_Template")]
    [PermissionTable(Name = "Contract_Template")]
    public partial class Contract_TemplateController : ApiBaseController<IContract_TemplateService>
    {
        public Contract_TemplateController(IContract_TemplateService service)
        : base(service)
        {
        }
    }
}

