/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果要增加方法请在当前目录下Partial文件夹Contract_MadeController编写
 */
using Microsoft.AspNetCore.Mvc;
using VOL.Core.Controllers.Basic;
using VOL.Entity.AttributeManager;
using VOL.AppManager.IServices;
namespace VOL.AppManager.Controllers
{
    [Route("api/Contract_Made")]
    [PermissionTable(Name = "Contract_Made")]
    public partial class Contract_MadeController : ApiBaseController<IContract_MadeService>
    {
        public Contract_MadeController(IContract_MadeService service)
        : base(service)
        {
        }
    }
}

