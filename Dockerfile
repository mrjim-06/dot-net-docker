FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /src
COPY . .

WORKDIR /src/VOL.WebApi
RUN dotnet restore "VOL.WebApi.csproj"
RUN dotnet publish "VOL.WebApi.csproj" -c Release -o out

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /src/VOL.WebApi
COPY --from=build-env /src/VOL.WebApi/out .
ENTRYPOINT ["dotnet", "VOL.WebApi.dll"]
