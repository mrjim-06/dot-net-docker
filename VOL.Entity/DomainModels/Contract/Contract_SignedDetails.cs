/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "合同明细",TableName = "Contract_SignedDetails",DetailTableCnName = "合同明细")]
    public class Contract_SignedDetails:BaseEntity
    {
        /// <summary>
       ///签订方
       /// </summary>
       [Display(Name ="签订方")]
       [Column(TypeName="int")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public int SideBy { get; set; }

       /// <summary>
       ///签署方名称
       /// </summary>
       [Display(Name ="签署方名称")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string SignatoryBy { get; set; }

       /// <summary>
       ///付款方
       /// </summary>
       [Display(Name ="付款方")]
       [Column(TypeName="sbyte")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public sbyte Payment { get; set; }

       /// <summary>
       ///联系人
       /// </summary>
       [Display(Name ="联系人")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string LiaisonMain { get; set; }

       /// <summary>
       ///联系电话
       /// </summary>
       [Display(Name ="联系电话")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string Mobile { get; set; }

       /// <summary>
       ///地址
       /// </summary>
       [Display(Name ="地址")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string Address { get; set; }

       /// <summary>
       ///银行账户
       /// </summary>
       [Display(Name ="银行账户")]
       [Column(TypeName="bigint")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public long BankAccount { get; set; }

       /// <summary>
       ///开户行
       /// </summary>
       [Display(Name ="开户行")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string OpeningBank { get; set; }

       /// <summary>
       ///签名
       /// </summary>
       [Display(Name ="签名")]
       [Column(TypeName="nvarchar(max)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string SignatureUrl { get; set; }

       /// <summary>
       ///付款人ID
       /// </summary>
       [Display(Name ="付款人ID")]
       [Column(TypeName="int")]
       [Editable(true)]
       public int? PayerId { get; set; }

       /// <summary>
       ///付款人
       /// </summary>
       [Display(Name ="付款人")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       [Editable(true)]
       public string Payer { get; set; }

       /// <summary>
       ///是否有效
       /// </summary>
       [Display(Name ="是否有效")]
       [Column(TypeName="sbyte")]
       [Editable(true)]
       public sbyte? Enable { get; set; }

       /// <summary>
       ///签订状态
       /// </summary>
       [Display(Name ="签订状态")]
       [Column(TypeName="int")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public int SignatureStatus { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Key]
       [Display(Name ="Made_Id")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int Made_Id { get; set; }

       /// <summary>
       ///
       /// </summary>
       [Display(Name ="Contract_Id")]
       [Column(TypeName="int")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public int Contract_Id { get; set; }

       /// <summary>
       ///签署时间
       /// </summary>
       [Display(Name ="签署时间")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? SignatoryDate { get; set; }

       /// <summary>
       ///甲方ID
       /// </summary>
       [Display(Name ="甲方ID")]
       [Column(TypeName="int")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public int SignatoryID { get; set; }

       /// <summary>
       ///人脸
       /// </summary>
       [Display(Name ="人脸")]
       [Column(TypeName="nvarchar(max)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string FaceUrl { get; set; }

       /// <summary>
       ///指纹
       /// </summary>
       [Display(Name ="指纹")]
       [Column(TypeName="nvarchar(max)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string FingerprintUrl { get; set; }

       
    }
}