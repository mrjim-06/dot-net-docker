/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "签订合同管理",TableName = "Contract_Made",DetailTable =  new Type[] { typeof(Contract_SignedDetails)},DetailTableCnName = "合同明细")]
    public class Contract_Made:BaseEntity
    {
        /// <summary>
       ///签订编号
       /// </summary>
       [Display(Name ="签订编号")]
       [MaxLength(20)]
       [Column(TypeName="nvarchar(20)")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public string MadeNo { get; set; }

       /// <summary>
       ///合同名称
       /// </summary>
       [Display(Name ="合同名称")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string Name { get; set; }

       /// <summary>
       ///合同类别
       /// </summary>
       [Display(Name ="合同类别")]
       [MaxLength(5)]
       [Column(TypeName="nvarchar(5)")]
       [Editable(true)]
       public string Type { get; set; }

       /// <summary>
       ///金额
       /// </summary>
       [Display(Name ="金额")]
       [DisplayFormat(DataFormatString="12,2")]
       [Column(TypeName="decimal")]
       [Editable(true)]
       public decimal? Money { get; set; }

       /// <summary>
       ///是否有效
       /// </summary>
       [Display(Name ="是否有效")]
       [Column(TypeName="sbyte")]
       [Editable(true)]
       public sbyte? Enable { get; set; }

       /// <summary>
       ///合同内容
       /// </summary>
       [Display(Name ="合同内容")]
       [Column(TypeName="nvarchar(max)")]
       [Editable(true)]
       public string Content { get; set; }

       /// <summary>
       ///主键
       /// </summary>
       [Key]
       [Display(Name ="主键")]
       [Column(TypeName="int")]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public int Contract_Id { get; set; }

       /// <summary>
       ///合同类别名称
       /// </summary>
       [Display(Name ="合同类别名称")]
       [MaxLength(100)]
       [Column(TypeName="nvarchar(100)")]
       [Editable(true)]
       public string TypeName { get; set; }

       /// <summary>
       ///创建人时间
       /// </summary>
       [Display(Name ="创建人时间")]
       [Column(TypeName="datetime")]
       public DateTime? CreateDate { get; set; }

       /// <summary>
       ///创建人
       /// </summary>
       [Display(Name ="创建人")]
       [Column(TypeName="int")]
       public int? CreateID { get; set; }

       /// <summary>
       ///创建者
       /// </summary>
       [Display(Name ="创建者")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       public string Creator { get; set; }

       [Display(Name ="合同明细")]
       [ForeignKey("Contract_Id")]
       public List<Contract_SignedDetails> Contract_SignedDetails { get; set; }

    }
}