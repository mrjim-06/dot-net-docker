/*
 *代码由框架生成,任何更改都可能导致被代码生成器覆盖
 *如果数据库字段发生变化，请在代码生器重新生成此Model
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOL.Entity.SystemModels;

namespace VOL.Entity.DomainModels
{
    [Entity(TableCnName = "合同范文管理",TableName = "Contract_Template")]
    public class Contract_Template:BaseEntity
    {
        /// <summary>
       ///序号
       /// </summary>
       [Key]
       [Display(Name ="序号")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int Id { get; set; }

       /// <summary>
       ///合同名称
       /// </summary>
       [Display(Name ="合同名称")]
       [MaxLength(200)]
       [Column(TypeName="nvarchar(200)")]
       [Editable(true)]
       public string Name { get; set; }

       /// <summary>
       ///合同类型
       /// </summary>
       [Display(Name ="合同类型")]
       [MaxLength(5)]
       [Column(TypeName="nvarchar(5)")]
       [Editable(true)]
       public string Type { get; set; }

       /// <summary>
       ///编号位数
       /// </summary>
       [Display(Name ="编号位数")]
       [Column(TypeName="int")]
       [MaxLength(1)]
       [Editable(true)]
       [Required(AllowEmptyStrings=false)]
       public int No { get; set; }

       /// <summary>
       ///创建时间
       /// </summary>
       [Display(Name ="创建时间")]
       [Column(TypeName="datetime")]
       [Editable(true)]
       public DateTime? CreateDate { get; set; }

       /// <summary>
       ///甲方(起草人)
       /// </summary>
       [Display(Name ="甲方(起草人)")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       [Editable(true)]
       public string Creator { get; set; }

       /// <summary>
       ///是否启用
       /// </summary>
       [Display(Name ="是否启用")]
       [Column(TypeName="sbyte")]
       [Editable(true)]
       public sbyte? Enable { get; set; }

       /// <summary>
       ///审核状态
       /// </summary>
       [Display(Name ="审核状态")]
       [Column(TypeName="int")]
       [Required(AllowEmptyStrings=false)]
       public int AuditStatus { get; set; }

       /// <summary>
       ///审核人
       /// </summary>
       [Display(Name ="审核人")]
       [MaxLength(20)]
       [Column(TypeName="nvarchar(20)")]
       public string Auditor { get; set; }

       /// <summary>
       ///合同内容
       /// </summary>
       [Display(Name ="合同内容")]
       [Column(TypeName="nvarchar(max)")]
       [Editable(true)]
       public string Content { get; set; }

       /// <summary>
       ///创建人ID
       /// </summary>
       [Display(Name ="创建人ID")]
       [Column(TypeName="int")]
       public int? CreateID { get; set; }

       /// <summary>
       ///最后修改人
       /// </summary>
       [Display(Name ="最后修改人")]
       [MaxLength(30)]
       [Column(TypeName="nvarchar(30)")]
       public string Modifier { get; set; }

       /// <summary>
       ///最后修改时间
       /// </summary>
       [Display(Name ="最后修改时间")]
       [Column(TypeName="datetime")]
       public DateTime? ModifyDate { get; set; }

       /// <summary>
       ///最后修改人ID
       /// </summary>
       [Display(Name ="最后修改人ID")]
       [Column(TypeName="int")]
       public int? ModifyID { get; set; }

       /// <summary>
       ///合同静态页
       /// </summary>
       [Display(Name ="合同静态页")]
       [MaxLength(255)]
       [Column(TypeName="nvarchar(255)")]
       public string DetailUrl { get; set; }

       /// <summary>
       ///审核时间
       /// </summary>
       [Display(Name ="审核时间")]
       [Column(TypeName="datetime")]
       public DateTime? AuditDate { get; set; }

       /// <summary>
       ///审核人ID
       /// </summary>
       [Display(Name ="审核人ID")]
       [Column(TypeName="int")]
       public int? AuditId { get; set; }

       
    }
}